FROM node:16-bullseye-slim AS nodejs-base

FROM nodejs-base AS build
ENV GANCIO_VERSION=1.5.6
ENV GANCIO_SRC_URL=https://gancio.org/releases/gancio-v${GANCIO_VERSION}.tgz
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -y install git yarnpkg && \
    env http_proxy= yarnpkg global add --latest --production --silent ${GANCIO_SRC_URL} && \
    apt-get clean && rm -fr /var/lib/apt/lists/*

FROM nodejs-base
COPY --from=build /usr/local/share/.config/yarn/ /usr/local/share/.config/yarn/
RUN ln -s ../share/.config/yarn/global/node_modules/.bin/gancio /usr/local/bin/gancio

ENTRYPOINT ["/usr/local/bin/gancio", "start", "--docker"]

